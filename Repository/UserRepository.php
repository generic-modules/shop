<?php // Repository/UserRepository.php

namespace Magnetar\ShopBundle\Repository;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Magnetar\CoreBundle\Repository\UserRepository as MagnetarCoreBundleUserRepository;
use Magnetar\ShopBundle\Entity\User;

class UserRepository extends MagnetarCoreBundleUserRepository
{
    /**
     * Class constructor
     * 
     * @param RegistryInterface $registry
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param string $userClass
     * 
     * @return void
     */
    public function __construct(RegistryInterface $registry, UserPasswordEncoderInterface $passwordEncoder, $userClass = User::class)
    {
        parent::__construct($registry, $passwordEncoder, $userClass);
    }
    
    /**
     * Search users by phrase.
     * 
     * @param string $phrase
     * 
     * @return mixed|\Doctrine\DBAL\Driver\Statement|array|NULL
     */
    public function searchByPhrase($phrase, $company)
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.employingCompany <> :company or u.employingCompany is null')->setParameter('company', $company->getId())
            ->andWhere('u.roles like :role')->setParameter('role', '%'.'ROLE_WORKER'.'%')
            ->andWhere('u.pesel like :phrase')->setParameter('phrase', '%'.$phrase.'%')
        ;
        return $qb->getQuery()->getResult();
    }
}
