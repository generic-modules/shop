#!/usr/bin/env bash
mysql -u root -proot -e "drop database if exists $MYSQL_DATABASE"
mysql -u root -proot -e "create database $MYSQL_DATABASE"
mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE < /db/init.sql
