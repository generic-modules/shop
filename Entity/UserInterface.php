<?php // Entity/UserInterface.php

namespace Magnetar\ShopBundle\Entity;

use Magnetar\CoreBundle\Entity\UserInterface as MagnetarCoreUserInterface;

interface UserInterface extends MagnetarCoreUserInterface
{
    /**
     * Worker role
     */    
    public const ROLE_WORKER = 'ROLE_WORKER';
    
    /**
     * Advanced worker role
     */
    public const ROLE_ADVANCED_WORKER = 'ROLE_ADVANCED_WORKER';
    
    /**
     * Company owner role
     */
    public const ROLE_COMPANY_OWNER = 'ROLE_COMPANY_OWNER';
    
    /**
     * New company assignment status
     */
    public const COMPANY_ASSIGNMENT_STATUS_ASSIGNED = 'COMPANY_ASSIGNMENT_STATUS_ASSIGNED';
    
    /**
     * Company assignment accepted status
     */
    public const COMPANY_ASSIGNMENT_STATUS_ACCEPTED = 'COMPANY_ASSIGNMENT_STATUS_ACCEPTED';
    
    /**
     * Company assignment rejected status
     */
    public const COMPANY_ASSIGNMENT_STATUS_REJECTED = 'COMPANY_ASSIGNMENT_STATUS_REJECTED';
}
